# APOIA.se Contrata Full Stack

### Quem somos?

Somos uma startup de inovação tecnológica, financeira e social. Trabalhamos fomentando a cultura do financiamento coletivo com e para comunidades e consideramos nossa missão de empoderar pessoas fazedoras tão importante quanto o serviço de intermediação financeira que prestamos. Se você é uma pessoa que busca desafios, gosta de um ambiente diverso e respeitoso, e tem sede por fazer e botar coisas nos mundo, venha conosco!

### Objetivo

1. Crie um repositório privado no bitbucket e nos (amora@apoia.se, victor@apoia.se) dê permissão de acesso.
2. Desenvolva o componente de inclusão de cartão de crédito.
    - Layout conforme as imagens fornecidas.
    - Deve-se permitir que um usuários possua no máximo 3 cartões de crédito registrados na nossa base.
    - Deve-se bloquear a ação em massa de usuários testadores de cartão de crédito.
3. Utilizamos métodos ágeis e kanban no nosso dia-a-dia, então utilize alguma ferramenta para gerenciar o andamento do projeto. Se for o caso, nos convide (amora@apoia.se, victor@apoia.se), pois queremos acompanhar o seu processo.

A avaliação é focada no componente de inclusão de cartão, então não se preocupe muito em reproduzir o cabeçalho do site.

#### Obrigatório

- React (https://reactjs.org/)
- Material UI (https://material-ui.com/)
- styled-jsx (https://github.com/zeit/styled-jsx) ou styled components (https://styled-components.com/)
- Testes (Nos mostre como você aplica a regra de negócio nos testes)
- Eslint (Airbnb)
- Mongodb

### O que será avaliado?

1. Se o objetivo foi executado com cuidado, qualidade e está funcional.
2. Se boas praticas foram utilizadas na resolução do objetivo.
3. Se há commits message claro, com o que foi feito.
4. Se foi realizada uma cobertura minima de testes.
5. A organização no trello.com durante o teste.
6. Se o layout foi bem observado e bem reproduzido.
7. A responsividade do projeto. **Mobile é muito importante!**
